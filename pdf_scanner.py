import re
from typing import Dict
from PyPDF2 import PdfFileReader
import pandas as pd

# pdf_file = open('./docs/pdf1.pdf', 'rb')
# pdf_reader = PyPDF2.PdfFileReader(pdf_file)

# # Thow this in for each page of pages
# page = pdf_reader.getPage(0)
# # then the format of the pages are the "Exact" same

# # So this pdf doesn't have actual fields and all that usefull stuff.
# # I don't know if thats an actual part of this "test" or just an error
# # Can still probably get the name and tax file number though
# text = page.extractText().replace('\n', '')
# Strip the stupid \n from everything

# "        PAYG Payment Summary  PAYG Payment Summary  PAYG Payment Summary          PAYG Payment Summary  PAYG Payment Summary          PAYG Payment Summary    PAYG payment summary - individual non-business  Payment Summary for year ending 30 June 2015  Payee details NOTICE TO PAYEE  If this payment summary shows an amount in the total  tax withheld box you must lodge a tax return. If no tax  Rory Pond was withheld you may still have to lodge a tax return.  PO Box 34   ERINA NSW 2250 For more information on whether you have to lodge, or   about this payment and how it is taxed, you can:    » visit www.ato.gov.au    » phone 13 28 61 between 8.00am and 6.00pm (EST)   Monday to Friday.   Day/Month/Year  Day/Month/Year  Period of payment 13/03/2015 to  30/06/2015           Payee's tax file number 456789231  TOTAL TAX WITHHELD $ 1134            Lump sum payments Type         Gross payments $ 10163 A $ 0                    CDEP payments $ 0 B $ 0                 Reportable fringe benefits amount FBT year 1 April to 31 March $ 0 D $ 0          Reportable employer superannuation contributions $ 0 E $ 0          Total allowances $ 0 Total allowances are not included in Gross payments above. This amount needs to be shown separately in your tax return.                             Payer details  Payer's ABN or withholding payer number  Branch number   Payer's name ACME Corp   Signature of authorised person  Date 30/06/2015    "
# Above is what is replaced.
# It keeps all of its formatting which makes it a pain in the butt

# and it looks like the name is after the phrase If no tax

# # \s+((?:\w+(?:\s+|$)){2})
# name = re.findall(r"\bIf no tax\b\s+((?:\w+(?:\s+|$)){2})", text)

# # And just for the sake of more data tax withheld
# tax_witheld = re.findall(r"\bTOTAL TAX WITHHELD\b \$ (\d+) ", text)

# # We can pretty safely assume that the tax file number is gonna be a set of 9 digits.
# tax_number = re.findall(r"\D(\d{9})\D", text)

# Lets just throw this all in a function so we can import it in main
def get_relevent_info() -> Dict[str, str]:
    """ Return all pii from a pdf file """
    pdf_file = './docs/pdf1.pdf'
    with open(pdf_file, 'rb') as f:
        pdf = PdfFileReader(f)
        # Throw in for loop
        page = pdf.getPage(0)
        # Throw in for loop
        text = page.extractText().replace('\n', '')
        name = re.findall(r"\bIf no tax\b\s+((?:\w+(?:\s+|$)){2})", text)
        tax_witheld = re.findall(r"\bTOTAL TAX WITHHELD\b \$ (\d+) ", text)
        tax_number = re.findall(r"\D(\d{9})\D", text)
        return { 'Name': name, 'Tax Number': tax_number, 'Tax Withheld' : tax_witheld }


def transform_pdf_results_to_data_frame():
    return pd.DataFrame(get_relevent_info(), columns=['Name', 'Tax Number', 'Tax Withheld'])


def write_to_xlsx():
    # Apparently panda doesn't like just appending as default
    writer = pd.ExcelWriter('pdfData.xlsx', engine='xlsxwriter')

    # Lets work around that, really quickly.
    workbook = writer.book
    worksheet = workbook.add_worksheet('pdfData')
    writer.sheets['pdfData'] = worksheet

    # Write a whole bunch
    transform_pdf_results_to_data_frame().to_excel(writer, 'pdfData', index=None)
    writer.save()
