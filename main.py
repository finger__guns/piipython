from csv_xslx_scanner import write_to_xlsx
from pdf_scanner import write_to_xlsx as pdf_excel_writer


def main():
    write_to_xlsx()
    pdf_excel_writer()


if __name__ == '__main__':
    main()
