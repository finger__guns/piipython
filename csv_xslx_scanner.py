import pandas as pd

csv_file = './docs/csv2.csv'
xlsx_file = './docs/spreadsheet1.xlsx'

# Name, Date of Birth, Address, Email Address and Contact Telephone number.
# In  other cases we work,
# this may also include Medicare Number, Drivers License, Tax File Number,
# Credit Card numbers, Passport details, Medical History, etc.

# We can pass in use col after we know what fields actually have values.
csv = pd.read_csv(csv_file, sep=',', usecols=['Family Name', 'Given Name', 'Date of Birth',
                                              'Gender', 'Tax File Number', 'Mobile Number',
                                              'Email Address', 'Address Line 1', 'Suburb',
                                              'State', 'Post Code', 'Country',
                                              'Employee Status'])
# Same as above, they are the same file esentially just different amount of values and
# values itself.
xlsx = pd.read_excel(xlsx_file, usecols=['Family Name', 'Given Name', 'Date of Birth',
                                              'Gender', 'Tax File Number', 'Mobile Number',
                                              'Email Address', 'Address Line 1', 'Suburb',
                                              'State', 'Post Code', 'Country',
                                              'Employee Status'])

def write_to_xlsx():
    # Apparently panda doesn't like just appending as default
    writer = pd.ExcelWriter('simple_out.xlsx', engine='xlsxwriter')

    # Lets work around that, really quickly.
    workbook = writer.book
    worksheet = workbook.add_worksheet('simpleSheet')
    writer.sheets['simpleSheet'] = worksheet

    csv.to_excel(writer, 'simpleSheet', startrow=0, startcol=0, index=None)
    xlsx.to_excel(writer, 'simpleSheet', startrow=5, header=None, index=None)
    writer.save()
